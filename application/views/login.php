<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/loginstyle.css'); ?> ">


<!--  <?php  if($this->session->flashdata('message')){  ?>
                 <div class="alert alert-danger">
        <?php echo $this->session->flashdata('message'); ?>
                </div>
        <?php  } ?>  -->

<div class="container ccontainer" style="padding-top: 5%;">
    <div class="row">
        <div class="col-sm-12 col-md-12 ">
           <div >
             
           
            
               
                <form class="form-signin" method="post" action="javascript:void(0)">

                <input type="text" class="form-control"  name="username" id="usern" placeholder="Email" required autofocus >
                <input type="password" class="form-control" name="password" id="passw" placeholder="Password" required autofocus >
       
        <p id="samm"></p>
      
                <button type="Submit" class="btn btn-lg btn-primary btn-block" id="loginn">Sign in</button> <!-- data-target="#loader" data-toggle="modal" -->
                
         

                
               
               <!--  <a href="#" class="pull-right need-help">Forgot Password </a><span class="clearfix"></span> -->
                </form>
                 <a data-target="#myModal" href="#myModal" data-toggle="modal" class="text-center new-account">Create an account </a>
               
            </div>
            </div>
           
        </div>
    </div>
</div>




<div class="modal" id="myModal" tabindex="-1" >
  <div class="modal-dialog" >
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Registration</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div id="reg_frm">
      <!--  <form method="post" action="<?php echo site_url("login/register/") ?> " id="reg_frm"> -->
           <div class="form-group">
      <label class="col-md-3 control-label" for="lastname">Last Name</label>
      <div class="col-md-9">
      <input id="lastname" name="lastname" type="text" pattern="[A-Za-z]{1,30}" placeholder="Your last name" class="form-control input-md" required="">
      <span class="help-block" id="help_last"></span>
      </div>
    </div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-3 control-label" for="firstname">First Name</label>  
  <div class="col-md-9">
  <input id="firstname" name="firstname" type="text" pattern="[A-Za-z]{1,30}" placeholder="Your first name" class="form-control input-md" required="">
    <span class="help-block" id="help_first"></span>
  </div>
</div>


<!-- Text input-->
<div class="form-group">
  <label class="col-md-3 control-label" for="username">Username</label>  
  <div class="col-md-9">
  <input id="username" name="username" type="text" placeholder="Your username" class="form-control input-md" required="">
    <span class="help-block" id="help_user"></span>
  </div>
</div>

<!-- Password input-->
<div class="form-group">
  <label class="col-md-3 control-label" for="password">Password </label>
  <div class="col-md-9">
    <input id="password" name="password" type="password" placeholder="Password" class="form-control input-md" required="">
    <span class="help-block" id="help_pass"></span>
  </div>
</div>

<!-- Password input-->
<div class="form-group">
  <label class="col-md-3 control-label" for="confirmasipassword">Confirm Password</label>
  <div class="col-md-9">
    <input id="confirmasipassword" name="confirmasipassword" type="password" placeholder="Confirmation password" class="form-control input-md" required="">
    <span class="help-block" id="help_cpass"></span>
  </div>
</div>
      
         
<!-- Multiple Radios (inline) -->

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-success" onclick="register()" id="regist">Register</button>
       <!--   </form> -->
      </div>
    </div>
  </div>
</div>


<!-- ####################################################### -->
<!-- #######################  loader      ################## -->
<!-- ####################################################### -->

<div class="modal" id="loader" tabindex="-1" role="dialog">
  <div class="" role="">
    <div class="">
      <div class="modal-body">
           <div class="lds-css ng-scope center">
        <div class="lds-spinner" style="height:100%"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
        <style type="text/css">@keyframes lds-spinner {
          0% {
            opacity: 1;
          }
          100% {
            opacity: 0;
          }
        }
        @-webkit-keyframes lds-spinner {
          0% {
            opacity: 1;
          }
          100% {
            opacity: 0;
          }
        }
        .lds-spinner {
          position: relative;
        }
        .lds-spinner div {
          left: 94px;
          top: 48px;
          position: absolute;
          -webkit-animation: lds-spinner linear 1s infinite;
          animation: lds-spinner linear 1s infinite;
          background: #ff727d;
          width: 12px;
          height: 24px;
          border-radius: 40%;
          -webkit-transform-origin: 6px 52px;
          transform-origin: 6px 52px;
        }
        .lds-spinner div:nth-child(1) {
          -webkit-transform: rotate(0deg);
          transform: rotate(0deg);
          -webkit-animation-delay: -0.916666666666667s;
          animation-delay: -0.916666666666667s;
        }
        .lds-spinner div:nth-child(2) {
          -webkit-transform: rotate(30deg);
          transform: rotate(30deg);
          -webkit-animation-delay: -0.833333333333333s;
          animation-delay: -0.833333333333333s;
        }
        .lds-spinner div:nth-child(3) {
          -webkit-transform: rotate(60deg);
          transform: rotate(60deg);
          -webkit-animation-delay: -0.75s;
          animation-delay: -0.75s;
        }
        .lds-spinner div:nth-child(4) {
          -webkit-transform: rotate(90deg);
          transform: rotate(90deg);
          -webkit-animation-delay: -0.666666666666667s;
          animation-delay: -0.666666666666667s;
        }
        .lds-spinner div:nth-child(5) {
          -webkit-transform: rotate(120deg);
          transform: rotate(120deg);
          -webkit-animation-delay: -0.583333333333333s;
          animation-delay: -0.583333333333333s;
        }
        .lds-spinner div:nth-child(6) {
          -webkit-transform: rotate(150deg);
          transform: rotate(150deg);
          -webkit-animation-delay: -0.5s;
          animation-delay: -0.5s;
        }
        .lds-spinner div:nth-child(7) {
          -webkit-transform: rotate(180deg);
          transform: rotate(180deg);
          -webkit-animation-delay: -0.416666666666667s;
          animation-delay: -0.416666666666667s;
        }
        .lds-spinner div:nth-child(8) {
          -webkit-transform: rotate(210deg);
          transform: rotate(210deg);
          -webkit-animation-delay: -0.333333333333333s;
          animation-delay: -0.333333333333333s;
        }
        .lds-spinner div:nth-child(9) {
          -webkit-transform: rotate(240deg);
          transform: rotate(240deg);
          -webkit-animation-delay: -0.25s;
          animation-delay: -0.25s;
        }
        .lds-spinner div:nth-child(10) {
          -webkit-transform: rotate(270deg);
          transform: rotate(270deg);
          -webkit-animation-delay: -0.166666666666667s;
          animation-delay: -0.166666666666667s;
        }
        .lds-spinner div:nth-child(11) {
          -webkit-transform: rotate(300deg);
          transform: rotate(300deg);
          -webkit-animation-delay: -0.083333333333333s;
          animation-delay: -0.083333333333333s;
        }
        .lds-spinner div:nth-child(12) {
          -webkit-transform: rotate(330deg);
          transform: rotate(330deg);
          -webkit-animation-delay: 0s;
          animation-delay: 0s;
        }
        .lds-spinner {
          width: 200px !important;
          height: 200px !important;
          -webkit-transform: translate(-100px, -100px) scale(1) translate(100px, 100px);
          transform: translate(-100px, -100px) scale(1) translate(100px, 100px);
        }
        </style></div>
      </div>
      
    </div>
  </div>
</div>